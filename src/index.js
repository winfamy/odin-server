import express from "express";
import { createServer } from "node:http";
import { join } from "node:path";
import { Server } from "socket.io";
import { getData } from "./db/get-data.js";
import { createItem } from "./db/create-item.js";
import { updateSystem } from "./db/update-system.js";
import "dotenv/config";
import {
  updateLaunchIntel,
  updateReentryIntel,
  updateConjunctionIntel,
} from "./db/update-intel.js";
import { createLaunch } from "./db/create-launch.js";
import { createConjunction } from "./db/create-conjunction.js";
import { createReentry } from "./db/create-reentry.js";

let CORS_HOST = process.env.CORS_HOST || "http://localhost:5173";
let PORT = process.env.PORT || 80;

const app = express();
const server = createServer(app);
const io = new Server(server, {
  cors: {
    origin: CORS_HOST,
  },
});

io.on("connection", async (socket) => {
  socket.emit("data", await getData());

  socket.on("add_data", async ({ data, country }) => {
    try {
      let parsedData = JSON.parse(atob(data));
      parsedData.source = country;
      let item = await createItem(parsedData);

      io.emit("new_data", {
        [parsedData.object_type]: item,
        object_type: parsedData.object_type,
      });
    } catch (err) {
      console.log(err);
    }
  });

  socket.on("update_system", async (id) => {
    let system = await updateSystem(id);
    io.emit("updated_system", { system });
  });

  socket.on("put_launch_intel", async (data) => {
    console.log(data);
    let launch = await updateLaunchIntel(data);
    io.emit("updated_launch", { launch });
  });

  socket.on("put_reentry_intel", async (data) => {
    console.log(data);
    let reentry = await updateReentryIntel(data);
    io.emit("updated_reentry", { reentry });
  });

  socket.on("put_conjunction", async (data) => {
    console.log(data);
    let conjunction = await updateConjunctionIntel(data);
    io.emit("updated_conjunction", { conjunction });
  });

  socket.on("create_launch", async ({ launch, country }) => {
    try {
      launch = await createLaunch({ launch, country });
      io.emit("new_data", { launch, object_type: "launch" });
    } catch (error) {
      console.log(error);
    }
  });

  socket.on("create_conjunction", async ({ conjunction, country }) => {
    try {
      conjunction = await createConjunction({ conjunction, country });
      io.emit("new_data", { conjunction, object_type: "conjunction" });
    } catch (error) {
      console.log(error);
    }
  });

  socket.on("create_reentry", async ({ reentry, country }) => {
    try {
      reentry = await createReentry({ reentry, country });
      io.emit("new_data", { reentry, object_type: "reentry" });
    } catch (error) {
      console.log(error);
    }
  });
});

server.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
  console.log("CORS Host: " + process.env.CORS_HOST ?? "http://localhost:5173");
});
