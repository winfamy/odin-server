import { Launch } from "./models/Launch.js";
export const createLaunch = ({ launch, country }) => {
  return Launch.create({
    timestamp: launch.timestamp,
    description: launch.description,
    site: launch.site,
    type: launch.type,
    country: launch.country,
    source: country,
    intel: launch.intel,
  });
};
