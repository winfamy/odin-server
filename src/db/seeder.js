import { sequelize } from "./sequelize.js";
import { Conjunction, Reentry, Launch, System } from "./models/index.js";
import moment from "moment";
import { faker } from "@faker-js/faker";
import {
  generateConjunction,
  generateLaunch,
  generateReentry,
} from "../db/generators.js";

const NUM_LAUNCHES = 10;
const NUM_REENTRIES = 5;
const NUM_CONJUNCTIONS = 5;

const tile_data = {
  grid: {
    SDA: [{ acronym: "SDA", status: "green" }],
    MW: [{ acronym: "AAA", status: "green" }],
    GRA: [{ acronym: "GRA", status: "green" }],
  },

  unordered: [
    // { acronym: "2LT", status: "green" },
    // { acronym: "GRA", status: "green" },
    // { acronym: "DYP", status: "green" },
    // { acronym: "HIL", status: "green" },
    // { acronym: "LIPS", status: "green" },
    // { acronym: "BBD", status: "green" },
    // { acronym: "BBD", status: "green" },
    // { acronym: "BBD", status: "green" },
    // { acronym: "BBD", status: "green" },
    // { acronym: "BBD", status: "green" },
    // { acronym: "BBD", status: "green" },
  ],
};

export const seed = async () => {
  // await System.drop();
  await System.sync({ force: true });
  await Launch.sync({ force: true });
  await Conjunction.sync({ force: true });
  await Reentry.sync({ force: true });

  let system_countries = ["us", "ca", "gb", "au", "nz", "fr", "de"];
  Object.keys(tile_data.grid).map((key) => {
    let acronym_group_items = tile_data.grid[key];
    system_countries.map((country) => {
      acronym_group_items.map((item) => {
        System.create({
          acronym: item.acronym,
          status: item.status,
          acronym_group: key,
          country,
        });
      });
    });
  });

  tile_data.unordered.map((item) => {
    system_countries.map((country) => {
      System.create({
        acronym: item.acronym,
        status: item.status,
        acronym_group: "",
        country,
      });
    });
  });

  for (let index = 0; index < NUM_LAUNCHES; index++) {
    let item = generateLaunch();
    Launch.create({
      timestamp: item.timestamp,
      description: item.description,
      site: item.site,
      type: item.type,
      country: item.country,
      source: item.source,
      intel: item.intel,
    });
  }

  for (let index = 0; index < NUM_CONJUNCTIONS; index++) {
    let item = generateConjunction();
    Conjunction.create({
      timestamp: item.timestamp,
      primary_id: item.primary_id,
      primary_name: item.primary_name,
      secondary_id: item.secondary_id,
      secondary_name: item.secondary_name,
      concern: item.concern,
      source: item.source,
    });
  }

  for (let index = 0; index < NUM_REENTRIES; index++) {
    let item = generateReentry();
    Reentry.create({
      timestamp: item.timestamp,
      object_id: item.object_id,
      object_name: item.object_name,
      type: item.type,
      source: item.source,
    });
  }
};
