import { Conjunction } from "./models/Conjunction.js";
import { Launch } from "./models/Launch.js";
import { Reentry } from "./models/Reentry.js";

export const updateLaunchIntel = async ({ id, intel }) => {
  const launch = await Launch.findByPk(id);
  console.log(launch);
  launch.intel = intel;
  await launch.save();
  return launch;
};

export const updateReentryIntel = async ({ id, intel }) => {
  const reentry = await Reentry.findByPk(id);
  console.log(reentry);
  reentry.intel = intel;
  await reentry.save();
  return reentry;
};

export const updateConjunctionIntel = async ({ id, intel }) => {
  const conjunction = await Conjunction.findByPk(id);
  console.log(conjunction);
  conjunction.intel = intel;
  await conjunction.save();
  return conjunction;
};
