import { Conjunction } from "./models/Conjunction.js";
import { Launch } from "./models/Launch.js";
import { Reentry } from "./models/Reentry.js";
import { System } from "./models/System.js";
import { sequelize } from "./sequelize.js";

export const getData = async function () {
  const systems = await System.findAll();
  const conjunctions = await Conjunction.findAll();
  const launches = await Launch.findAll();
  const reentries = await Reentry.findAll();

  return {
    systems,
    conjunctions,
    launches,
    reentries,
  };
};
