import { DataTypes } from "sequelize";
import { sequelize } from "../sequelize.js";

export const Launch = sequelize.define("Launch", {
  timestamp: DataTypes.DATE,
  description: DataTypes.STRING,
  site: DataTypes.STRING,
  type: DataTypes.STRING,
  country: DataTypes.STRING,
  source: DataTypes.STRING,
  intel: DataTypes.STRING,
});
