import { DataTypes } from "sequelize";
import { sequelize } from "../sequelize.js";

export const Conjunction = sequelize.define("Conjunction", {
  timestamp: DataTypes.DATE,
  primary_id: DataTypes.STRING,
  primary_name: DataTypes.STRING,
  secondary_id: DataTypes.STRING,
  secondary_name: DataTypes.STRING,
  concern: DataTypes.STRING,
  source: DataTypes.STRING,
  intel: DataTypes.STRING,
});
