import { DataTypes } from "sequelize";
import { sequelize } from "../sequelize.js";

export const System = sequelize.define("System", {
  country: DataTypes.STRING,
  acronym: DataTypes.STRING,
  acronym_group: DataTypes.STRING,
  status: DataTypes.STRING,
});
