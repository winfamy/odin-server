import { DataTypes } from "sequelize";
import { sequelize } from "../sequelize.js";

export const Reentry = sequelize.define("Reentry", {
  timestamp: DataTypes.DATE,
  object_id: DataTypes.STRING,
  object_name: DataTypes.STRING,
  type: DataTypes.STRING,
  source: DataTypes.STRING,
  intel: DataTypes.STRING,
});
