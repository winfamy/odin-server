import { Conjunction } from "./models/Conjunction.js";
import { Launch } from "./models/Launch.js";
import { Reentry } from "./models/Reentry.js";
import { System } from "./models/System.js";
export const createItem = async function (item) {
  switch (item.object_type) {
    case "reentry":
      return await Reentry.create({
        timestamp: item.timestamp,
        object_id: item.object_id,
        object_name: item.object_name,
        type: item.type,
        source: item.source,
        intel: item.intel,
      });

      break;
    case "launch":
      return await Launch.create({
        timestamp: item.timestamp,
        description: item.description,
        site: item.site,
        type: item.type,
        country: item.country,
        source: item.source,
        intel: item.intel,
      });

      break;
    case "conjunction":
      return await Conjunction.create({
        timestamp: item.timestamp,
        primary_id: item.primary_id,
        primary_name: item.primary_name,
        secondary_id: item.secondary_id,
        secondary_name: item.secondary_name,
        concern: item.concern,
        source: item.source,
        intel: item.intel,
      });

      break;
    default:
      console.log("wat");
  }
};
