import { faker } from "@faker-js/faker";
import moment from "moment";

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min); // The maximum is exclusive and the minimum is inclusive
}

function getSource() {
  let countries = ["us", "gb", "au", "nz", "ca", "fr", "de"];
  return countries[Math.floor(Math.random() * countries.length)];
}

function getDate() {
  return faker.date.between({
    from: moment().subtract(5, "days"),
    to: moment().add(15, "days"),
  });
}

export function generateConjunction() {
  let description =
    faker.company.buzzVerb().toUpperCase() + "-" + faker.string.numeric(1);
  let primary_id = faker.string.numeric({ length: 6, allowLeadingZeros: true });
  let primary_name =
    faker.location.state({ abbreviated: true }) +
    faker.location.state({ abbreviated: true }) +
    "-" +
    faker.string.numeric(3);

  let secondary_id = faker.string.numeric({
    length: 6,
    allowLeadingZeros: true,
  });
  let secondary_name =
    faker.location.state({ abbreviated: true }) +
    faker.location.state({ abbreviated: true }) +
    "-" +
    faker.string.numeric(3);

  let type = "payload";
  let source = getSource();
  let timestamp = getDate();

  let intel = faker.lorem.lines(4);
  let concern =
    getRandomInt(0, 3) === 0
      ? ["low", "medium", "high", "emergency"][getRandomInt(0, 4)]
      : "";

  return {
    object_type: "conjunction",
    timestamp,
    concern,
    primary_id,
    primary_name,
    secondary_id,
    secondary_name,
    source,
    intel,
  };
}

export function generateLaunch() {
  let description =
    faker.company.buzzVerb().toUpperCase() + "-" + faker.string.numeric(1);
  let site =
    faker.location.state({ abbreviated: true }) +
    faker.location.state({ abbreviated: true }) +
    "-" +
    faker.string.numeric(3);
  let type = faker.airline.airplane().name;
  let country = faker.location.countryCode().toLowerCase();
  let source = getSource();
  let timestamp = getDate();

  let intel = faker.lorem.lines(20);

  return {
    object_type: "launch",
    site,
    timestamp,
    description,
    type,
    country,
    source,
    intel,
  };
}

export function generateReentry() {
  let description =
    faker.company.buzzVerb().toUpperCase() + "-" + faker.string.numeric(1);
  let object_id = faker.string.numeric({ length: 6, allowLeadingZeros: true });
  let object_name =
    faker.location.state({ abbreviated: true }) +
    faker.location.state({ abbreviated: true }) +
    "-" +
    faker.string.numeric(3);
  let type = "payload";
  let source = getSource();
  let timestamp = getDate();
  let intel = faker.lorem.lines(4);

  return {
    object_type: "reentry",
    timestamp,
    object_id,
    object_name,
    type,
    source,
    intel,
  };
}
