import { System } from "./models/System.js";

export const updateSystem = async function (id) {
  let item = await System.findByPk(id);
  if (!item) return;

  if (item.status === "red") {
    item.status = "green";
  } else if (item.status === "yellow") {
    item.status = "red";
  } else if (item.status === "green") {
    item.status = "yellow";
  }
  await item.save();

  return item.dataValues;
};
