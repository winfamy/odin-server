import { Reentry } from "./models/Reentry.js";
export const createReentry = ({ reentry, country }) => {
  return Reentry.create({
    timestamp: reentry.timestamp,
    object_id: reentry.object_id,
    object_name: reentry.object_name,
    type: reentry.type,
    intel: reentry.intel,
    source: country,
  });
};
