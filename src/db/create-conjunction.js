import { Conjunction } from "./models/Conjunction.js";
export const createConjunction = ({ conjunction, country }) => {
  return Conjunction.create({
    timestamp: conjunction.timestamp,
    primary_id: conjunction.primary_id,
    primary_name: conjunction.primary_name,
    secondary_id: conjunction.secondary_id,
    secondary_name: conjunction.secondary_name,
    concern: conjunction.concern,
    intel: conjunction.intel,
    source: country,
  });
};
