import {
  generateConjunction,
  generateReentry,
  generateLaunch,
} from "../src/db/generators.js";

export function sampleData() {
  console.log("-- LAUNCH --");
  for (let index = 0; index < 2; index++) {
    let obj = generateLaunch();
    console.log(btoa(JSON.stringify(obj)));
  }
  console.log();

  console.log("-- REENTRIES --");
  for (let index = 0; index < 2; index++) {
    let obj = generateReentry();
    console.log(btoa(JSON.stringify(obj)));
  }
  console.log();

  console.log("-- CONJUNCTIONS --");
  for (let index = 0; index < 2; index++) {
    let obj = generateConjunction();
    console.log(btoa(JSON.stringify(obj)));
  }
}

sampleData();
